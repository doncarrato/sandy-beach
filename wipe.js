
// NOTE: Accounts that are not represented in your /users node will not be deleted!
"use strict";
// Get credentials and initialize firebase app
console.log("With the power vested in the admin user, lay waste the database and all its users!");
let admin = require("firebase-admin");
let serviceAccount = require([__dirname, "service-account-key.json"].join('/'));
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://your-database-name-here.firebaseio.com"
});
// Fetch the /users node and begin deleting users.
// Finally, wipe the database.
let user, users = [];
let usersRef = admin.database().ref('/users');
usersRef.once('value').then( (snapshot) => {
    // Get an Array of users from the DatabaseSnapshot
    snapshot.forEach( (childSnapshot) => {
        user = childSnapshot.val();
        users.push(user);
    });
    console.log(users.length + " users retrieved");
    // Delete users then wipe the database
    if (users.length > 0) {
        // Map users to an Array of Promises
        console.log("Delete users... ");
        let promises = users.map( user => deleteUser(user) );
        // Wait for all Promises to complete before wiping db
        Promise.all(promises)
            .then(wipeDatabase)
            .catch( e => console.log(e.message) );
    } else {
        // If no users to delete, then just wipe database
    }
    // Delete a user
    // Promise always resolves, even if user doesn't exist,
    // since a user can be deleted separately from their /users node
    function deleteUser(user) {
        return new Promise((resolve, reject) => {
            console.log("Delete user: " + user.name + "");
            admin.auth()
                .deleteUser(user.uid)
                .then( () => {
                    console.log(user.name + " deleted.");
                    resolve(user);
                })
                .catch( e => {
                    console.log([e.message, user.name, "could not be deleted!"].join(' '));
                    resolve(user);
                });
        });
    }

});