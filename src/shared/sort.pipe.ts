
import {Injectable, Pipe, PipeTransform} from '@angular/core';
import { User } from '../app/users/user/user';

@Pipe({
    name: 'sort',
    pure:false
})
@Injectable()
export class SortPipe implements PipeTransform {
    
    transform(value:User[], field:string, sortDir:boolean): any {
        if(!value || !field)
            return value;
        if(value.length === 0 || field === '')
            return value;
        value.sort((a: any, b: any) => {
            if (a[field].toLowerCase() < b[field].toLowerCase( )) {
              return sortDir? -1 : 1;
            } else if (a[field].toLowerCase( ) > b[field].toLowerCase( )) {
              return sortDir? 1 : -1;
            } else {
              return 0;
            }
          });
          return value;
        }
}