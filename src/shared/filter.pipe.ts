import {Injectable, Pipe, PipeTransform} from '@angular/core';
import { User } from '../app/users/user/user';

@Pipe({
    name: 'filter',
    pure:false
})
@Injectable()
export class FilterPipe implements PipeTransform {
    transform(value:User[], filterString:string,): any {
        if(!value || !filterString)
            return value;
        if(value.length === 0 || filterString === '')
          return value;
        const resultArray = [];
        for (const item of value) {
          if(item.FirstName.toLowerCase().includes(filterString.toLowerCase()) ||
          item.LastName.toLowerCase().includes(filterString.toLowerCase()) ||
          item.Spouse.toLowerCase().includes(filterString.toLowerCase()) ||
          item.Title.toLowerCase().includes(filterString.toLowerCase())  
            ){
            resultArray.push(item);
          }
        }
        return resultArray;
    
      }
}