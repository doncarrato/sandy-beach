import { Http, Response } from "@angular/http";
import { Injectable } from "@angular/core";
import { User } from "./users/user/user";
import { Subject } from "rxjs/Subject";
import 'rxjs/Rx';
import { AuthService } from "./auth.service";
@Injectable()
export class UserService {

    addingUser = false;
    editingUser:User;
    usersChanged = new Subject();
    constructor(private http:Http, private authService:AuthService) {
    }

    getUsers(){
        const token = this.authService.getToken();

        return this.http.get('https://sbyc-gi.firebaseio.com/users.json?auth='+token)
            .map((response:Response)=>{
                let data = <User[]>response.json();
                let users = new Array<User>();
                data.forEach(element => {
                    var u = new User(element.Title,element.FirstName,element.LastName,element.Spouse,element.HomePhone,element.CellPhone,element.SpousePhone, element.address1,element.address2,element.Email,element.SpouseEmail,element.ImageVersion,element.WO,element.BoatMM,element.BoatName,element.MemberYear,element.MemberNumber, element.MemberType);
                    u.id = element.id;
                    u.Anniversary = element.Anniversary;
                    u.BoatMM2 = element.BoatMM2;
                    u.BoatName2 = element.BoatName2;
                    u.BoatOwner= element.BoatOwner;
                    u.DOB = element.DOB
                    u.OtherAddress1 = element.OtherAddress1;
                    u.OtherAddress2 = element.OtherAddress2;
                    u.SpouseDOB = element.SpouseDOB;
                    users.push(u)
                });
                return users;
            });
    }

    setUsers(users:User[])
    {
        const token = this.authService.getToken();
        this.http.put('https://sbyc-gi.firebaseio.com/users.json?auth='+token,users).subscribe(()=>{
            this.usersChanged.next();
        });
    }
}