import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UserComponent } from './users/user/user.component';
import { UserService } from './users.service';
import { FilterPipe } from '../shared/filter.pipe';
import { UserDetailComponent } from './users/user-detail/user-detail.component';
import { AuthService } from './auth.service';
import { AuthGuard } from './auth-guard.service';
import { SigninComponent } from './signin/signin.component';
import { AdminAuthGuard } from './admin-auth-guard.service';
import { HeaderComponent } from './header/header.component';
import { SortPipe } from '../shared/sort.pipe';

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserComponent,
    FilterPipe,
    SortPipe,
    UserDetailComponent,
    SigninComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    AppRoutingModule    
  ],
  providers: [UserService, AuthService, AuthGuard,AdminAuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
