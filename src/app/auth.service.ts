import { Router } from '@angular/router';
import * as firebase from 'firebase';
import { Injectable } from '@angular/core';
import { User } from './users/user/user';
import { UserService } from './users.service';
import { Http, Response } from '@angular/http';

@Injectable()
export class AuthService {
  token: string;
  users:User[];
  firebaseApp;
  secondaryApp;
  constructor(private router: Router, private http:Http) {this.loadFirebase();}

  loadFirebase() {
    this.firebaseApp = firebase.initializeApp({
      apiKey: "AIzaSyAIRw6m1Agnt6ubSZqRK9QhcjKwu-zV5vs",
      authDomain: "sbyc-gi.firebaseapp.com",
      storageBucket:'sbyc-gi.appspot.com'
    });

    var config = {
      apiKey: "AIzaSyAIRw6m1Agnt6ubSZqRK9QhcjKwu-zV5vs",
      authDomain: "sbyc-gi.firebaseapp.com",
      storageBucket:'sbyc-gi.appspot.com'
    };

    this.secondaryApp = firebase.initializeApp(config, "Secondary");
  }

  signupUser(email: string, password: string) {
    return  this.secondaryApp.auth().createUserWithEmailAndPassword(email, password)
      .catch(
        error => console.log(error)
      )     
  }

  signinUser(email: string, password: string) {
    this.firebaseApp.auth().signInWithEmailAndPassword(email, password)
      .then(
        response => {
          this.router.navigate(['/users']);
          firebase.auth().currentUser.getIdToken()
            .then(
              (token: string) => this.token = token
            )
        }
      )
      .catch(
        error => console.log(error)
      );
  }

  setUser(user:User){
      this.secondaryApp.auth().signInWithEmailAndPassword(user.MemberNumber + "@sbyc.net","SBYC123")
                .then(
                  response => {
                    user.id = firebase.auth().currentUser.uid;
                    this.http.patch('https://sbyc-gi.firebaseio.com/users.json', user).subscribe();
                  });
  }

  setUsers(){
    let localUsers:User[] = [];
    return this.http.get('https://sbyc-gi.firebaseio.com/users.json')
        .subscribe((response:Response)=>{
            let data = response.json();
            this.users = data;
            this.users.forEach((user,i) => {
              setTimeout(()=>
                firebase.auth().signInWithEmailAndPassword(user.MemberNumber + "@sbyc.net","SBYC123")
                .then(
                  response => {
                    user.id = firebase.auth().currentUser.uid;
                    localUsers.push(user);
                    this.http.put('https://sbyc-gi.firebaseio.com/users.json', localUsers).subscribe();
                  })
                ,500*i)
            });
        });
  }


  createAllUsers(users:User[]){
    for (let index = 98; index < users.length; index++) {
      const user = users[index];
      setTimeout(()=>{
        this.signupUser(user.MemberNumber + "@sbyc.net","SBYC123" );
      }
      ,1000*(index-97));
    }
    //users.forEach((user,i) => {      
      // firebase.auth().signInWithEmailAndPassword(user.MemberNumber + "@sbyc.net","SBYC123")
      // .then(
      //   response => {
      //     user.id =  firebase.auth().currentUser.uid
      //     users.push(user);
      //   }
      // )
      // .catch(
      //   error => console.log(error)
      // );
    //});
  }

  logout() {
    this.firebaseApp.auth().signOut();
    this.token = null;
  }

  getToken() {
    this.firebaseApp.auth().currentUser.getIdToken()
      .then(
        (token: string) => this.token = token
      );
    return this.token;
  }

  isAuthenticated() {
    return this.token != null;
  }

  isAdmin(){
    return this.firebaseApp.auth().currentUser.email == "admin@sbyc.net";
  }

  isUserAccount(userId:string){
    return this.firebaseApp.auth().currentUser.email == (userId + "@sbyc.net") || this.isAdmin();
  }
}
