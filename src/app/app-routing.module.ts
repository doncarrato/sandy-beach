import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { UserDetailComponent } from "./users/user-detail/user-detail.component";
import { UsersComponent } from "./users/users.component";
import { AuthGuard } from "./auth-guard.service";
import { SigninComponent } from "./signin/signin.component";
import { AdminAuthGuard } from "./admin-auth-guard.service";
//import { RecipeDetailComponent } from "./recipes/recipe-detail/recipe-detail.component";
//import { RecipeStartComponent } from "./recipes/recipe-start/recipe-start.component";
//import { RecipeEditComponent } from "./recipes/recipe-edit/recipe-edit.component";
//import { SignupComponent } from "./auth/signup/signup.component";
//import { SigninComponent } from "./auth/signin/signin.component";

const appRoutes: Routes 
= [
    {path: '', component:SigninComponent},
    {path:'users',component:UsersComponent, canActivate: [AuthGuard]},
    {path:'edit', component:UserDetailComponent, canActivate: [AuthGuard]}
    // {path:'Recipes', component:RecipesComponent, children:[
    //     {path:'', component:RecipeStartComponent} ,
    //     {path:'new', component:RecipeEditComponent},
    //     {path:':id', component:RecipeDetailComponent},    
    //     {path:':id/edit', component:RecipeEditComponent},    
    // ]},
    // {path:'signup', component:SignupComponent},
    // {path:'signin', component:SigninComponent},
];

@NgModule({
    imports:[RouterModule.forRoot(appRoutes)],
    exports:[RouterModule]
})
export class AppRoutingModule {
    
}