import { Component, OnInit, Input } from '@angular/core';
import { User } from './user';
import { UserService } from '../../users.service';
import { Router } from '@angular/router';
import { AuthService } from '../../auth.service';
import { LowerCasePipe } from '@angular/common';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  @Input() user: User;
  @Input() index:number;

  showDetail: boolean;
  constructor(private userService:UserService, private router:Router, public authService:AuthService) { }

  ngOnInit() {
    this.showDetail = false;
  }

  getVersion(){
    return this.user.ImageVersion;
  }
  onEditItem(user:User)
  {
    this.userService.editingUser = user;
    this.userService.addingUser = false;
    this.router.navigate(['/edit'])
    
  }
  IsWO(user:User)
  {
      return (user as User).IsWO(); 
  }
}
