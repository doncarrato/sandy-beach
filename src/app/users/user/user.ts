
export class User {

    public TitleTrimmed(){
        if(!this.Title.toLowerCase().includes("mr") && !this.Title.toLowerCase().includes("ms"))
            return this.Title;
        else
            return ""; 
    }

    public IsWO() {
        if(typeof(this.WO) == "string")
        {
            if(this.WO == "false")
            {
                return "";
            }
            else if(this.WO == "true")
            {
                return "W/O";
            }
            else if(this.WO.toLowerCase() == "no")
            {
                return "";
            }
            else
            {
                return "W/O";
            }
        }
        else
            return this.WO ? "W/O" : "";
    }

    public  id:string;
    public  Anniversary:string;
    public  BoatMM2: string;
    public  BoatName2:string;
    public  BoatOwner:string;
    public  DOB:string;
    public  OtherAddress1:string;
    public  OtherAddress2:string;
    public  SpouseDOB:string;
    public  Salutation:string;

    
    constructor(
        public  Title:string,
        public  FirstName:string,
        public  LastName:string,
        public  Spouse:string,
        public  HomePhone:string,
        public  CellPhone:string,
        public  SpousePhone:string,
        public  address1:string,
        public  address2:string,
        public  Email:string,
        public  SpouseEmail:string,
        public  ImageVersion:number,
        public  WO:string,
        public  BoatMM:string,
        public  BoatName:string,
        public  MemberYear:string,
        public  MemberNumber:number,
        public  MemberType:string
    ) {}

}