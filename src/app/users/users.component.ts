import { Component, OnInit } from '@angular/core';
import { User } from './user/user';
import { UserService } from '../users.service';
import { Response } from '@angular/http';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users:User[];
  textValue:string;
  sortValue:string;
  sortDir:boolean;
  constructor(private userService:UserService, private router:Router, public authService:AuthService) { }

  ngOnInit() {
    this.updateUsers()
    this.sortValue = "LastName";
    this.sortDir=true;
      this.userService.usersChanged.subscribe(()=>{
      this.updateUsers()
    })
  }
  addUser() {
    this.userService.addingUser = true;
    this.router.navigate(['/edit']);
  }

  updateUsers() {
    this.userService.getUsers()        
    .subscribe((users:User[])=>{        
      this.users=users;
      this.users.sort((a,b)=>{
        var nameA = a.LastName.toUpperCase(); // ignore upper and lowercase
        var nameB = b.LastName.toUpperCase(); // ignore upper and lowercase
        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;
        }        })
    });


  }

}
