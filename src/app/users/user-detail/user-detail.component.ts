import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators, NgForm, ReactiveFormsModule } from '@angular/forms';
import { UserService } from '../../users.service';
import { Response } from '@angular/http';
import { User } from '../user/user';
import { Router } from '@angular/router';
import { AuthService } from '../../auth.service';
import * as firebase  from 'firebase';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {
  userForm : FormGroup;
  name:string;
  user: User;
  constructor( public  userService:UserService,public authService:AuthService, private router:Router) { }

  ngOnInit() {
    this.user = this.userService.editingUser;
    if(!this.user)
      this.userService.getUsers().subscribe((u=>{
        var max = Math.max.apply(Math,u.map(function(o){return o.MemberNumber;}))
        this.user = new User("","","","","","","","","","","",0,"false","","","",max+1,"");
        this.userForm = new FormGroup({
        'title': new FormControl(this.user.Title,Validators.required),
        'firstName': new FormControl(this.user.FirstName,Validators.required),
        'lastName': new FormControl(this.user.LastName,Validators.required),
        'spouse': new FormControl(this.user.Spouse),
        'wo': new FormControl(this.user.WO),
        'homePhone': new FormControl(this.user.HomePhone),
        'cellPhone': new FormControl(this.user.CellPhone),
        'spousePhone': new FormControl(this.user.SpousePhone),
        'address1': new FormControl(this.user.address1,Validators.required),
        'address2': new FormControl(this.user.address2,Validators.required),
        'email': new FormControl(this.user.Email),
        'spouseEmail': new FormControl(this.user.SpouseEmail),
        'boatMM': new FormControl(this.user.BoatMM),
        'boatName': new FormControl(this.user.BoatName),
        'memberYear': new FormControl(this.user.MemberYear),
        'memberNumber': new FormControl(this.user.MemberNumber),
        'memberType': new FormControl(this.user.MemberType)       
      });
    }));
    else
    this.userForm = new FormGroup({
      'title': new FormControl(this.user.Title,Validators.required),
      'firstName': new FormControl(this.user.FirstName,Validators.required),
      'lastName': new FormControl(this.user.LastName,Validators.required),
      'spouse': new FormControl(this.user.Spouse),
      'wo': new FormControl(this.user.WO),
      'homePhone': new FormControl(this.user.HomePhone),
      'cellPhone': new FormControl(this.user.CellPhone),
      'spousePhone': new FormControl(this.user.SpousePhone),
      'address1': new FormControl(this.user.address1,Validators.required),
      'address2': new FormControl(this.user.address2,Validators.required),
      'email': new FormControl(this.user.Email),
      'spouseEmail': new FormControl(this.user.SpouseEmail),
      'boatMM': new FormControl(this.user.BoatMM),
      'boatName': new FormControl(this.user.BoatName),
      'memberYear': new FormControl(this.user.MemberYear),
      'memberNumber': new FormControl(this.user.MemberNumber),
      'memberType': new FormControl(this.user.MemberType)    
    });
  
      
  }
  onCancel(){
    this.userService.editingUser = null;
    this.router.navigate(['/users']);
  }
  onDelete()
  {
    var result = confirm("Are you sure you want to delete?");
    if (result) {
      let users:User[];
      this.userService.getUsers().subscribe((u=>{
        users = <User[]>u; 
        let index = users.findIndex((obj => obj.id == this.userService.editingUser.id));
        if (index > -1) {
          users.splice(index, 1);
        }
        this.userService.setUsers(users);
        this.onCancel();
  
      }));
    }
  }

  save(user:User){
    let users:User[];
    this.userService.getUsers().subscribe((u=>{
      users = <User[]>u; 

      if(!this.userService.addingUser)
        user.MemberNumber= this.user.MemberNumber;
      else
        user.MemberNumber= Math.max.apply(Math,users.map(function(o){return o.MemberNumber;})) + 1;

      if(!this.userService.addingUser)
      {
        let index = users.findIndex((obj => obj.MemberNumber == this.user.MemberNumber));
        users[index] = user;
        this.userService.setUsers(users);
        this.onCancel();
        }
      else
      {
        this.authService.signupUser(user.Email,"SBYC123").then(()=>{
          user.id = this.authService.secondaryApp.auth().currentUser.uid;
          users.push(user);
          this.userService.setUsers(users);
          //this.authService.setUser(user)
          this.onCancel();
        }); 
        
      }
    }));

  }

  resetPassword(){
    var auth = firebase.auth();
    return auth.sendPasswordResetEmail(this.user.Email)
      .then(() =>  alert("Email Sent"))
      .catch((error) => console.log(error))
  }

  onSubmit(){
    const user = new User(
      this.userForm.value['title'],
      this.userForm.value['firstName'],
      this.userForm.value['lastName'],
      this.userForm.value['spouse'],
      this.userForm.value['homePhone'],
      this.userForm.value['cellPhone'],
      this.userForm.value['spousePhone'],
      this.userForm.value['address1'],
      this.userForm.value['address2'],
      this.userForm.value['email'],
      this.userForm.value['spouseEmail'],
      this.user.ImageVersion != null ? this.user.ImageVersion +1 : 1,
      this.userForm.value['wo'],
      this.userForm.value['boatMM'],
      this.userForm.value['boatName'],
      this.userForm.value['memberYear'],
      this.userForm.value['memberNumber'],
      this.userForm.value['memberType'],
      
    );
    user.Salutation = this.userForm.value['salutation'];
    let image = (<HTMLInputElement>document.getElementById('imageUpload')).files[0]
    let storageRef = firebase.storage().ref();
    let iref = storageRef.child("/members/"+this.user.MemberNumber);
    if(image!=null)
    {
      iref.put(image).then(()=>{
        this.save(user);
      });
    }
    else
      this.save(user);
  }
}
