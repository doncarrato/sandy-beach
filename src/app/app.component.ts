import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { AuthService } from './auth.service';
import { UserService } from './users.service';
import { User } from './users/user/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  constructor()//public authService:AuthService, public userService:UserService)
  {}

  ngOnInit() {

  }

  // onCreateAllUsers(){
  //   this.userService.getUsers().subscribe((users:User[])=> {
  //     this.authService.createAllUsers(users);
  //   });
  // }

  // onSetAllUsers() {
  //   this.authService.setUsers();
  // }
}
